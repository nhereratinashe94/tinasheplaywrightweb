package Test;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.CartModel;
import models.CheckOutModel;
import models.InventoryPageModel;
import models.LogInPageModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckOutTest {

    Playwright playwright = Playwright.create();
    Browser browser = playwright.chromium().launch((new BrowserType.LaunchOptions().setHeadless(true)));
    Page page = browser.newPage();

    @Before
    public void setUp() {
        page.navigate("https://www.saucedemo.com/");
        LogInPageModel logInPage = new LogInPageModel(page);
        CartModel cartModel = new CartModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);

        //log in
        logInPage.logIn("standard_user", "secret_sauce");

        //Inventory Page
        inventoryPageModel.checkObjectText(inventoryPageModel.title, "PRODUCTS",true);
         //add to cart
        inventoryPageModel.addToCart("labs-backpack");
        inventoryPageModel.clickObject(inventoryPageModel.cartButton);
        cartModel.checkCart(  "1","Sauce Labs Backpack");
        cartModel.clickObject(cartModel.checkOutButton);


    }
    @Test
    public void userIsAbleToAddPersonalInformation_andProgressToCheckOutOverview() {
        CheckOutModel checkOutModel = new CheckOutModel(page);
        checkOutModel.checkObjectText(checkOutModel.title, "CHECKOUT: YOUR INFORMATION",true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldFirstName,true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldLastName,true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldZipCode,true);
        checkOutModel.addUserInfo("Tinashe","Nherera","hd74az");
        checkOutModel.clickObject(checkOutModel.continueButton);
        checkOutModel.checkObjectText(checkOutModel.title, "CHECKOUT: OVERVIEW",true);
    }
    @Test
    public void userIsAbleToReviewOrderAndFinishOrder() {
        CheckOutModel checkOutModel = new CheckOutModel(page);
        checkOutModel.checkObjectText(checkOutModel.title, "CHECKOUT: YOUR INFORMATION",true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldFirstName,true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldLastName,true);
        checkOutModel.checkVisibility(checkOutModel.inputFieldZipCode,true);
        checkOutModel.addUserInfo("Tinashe","Nherera","hd74az");
        checkOutModel.clickObject(checkOutModel.continueButton);

        //check overview
        checkOutModel.checkObjectText(checkOutModel.title, "CHECKOUT: OVERVIEW",true);
        checkOutModel.checkObjectText(checkOutModel.inventoryName, "Sauce Labs Backpack",true);
        checkOutModel.checkObjectText(checkOutModel.quantity, "1",true);
        checkOutModel.clickObject(checkOutModel.finishButton);

        //checkout complete
        checkOutModel.checkObjectText(checkOutModel.title, "CHECKOUT: COMPLETE!",true);
        checkOutModel.checkObjectText(checkOutModel.orderCompleteHeader, "THANK YOU FOR YOUR ORDER",true);

    }

    @After
    public void closeBrowser(){
        browser.close();
    }

}
