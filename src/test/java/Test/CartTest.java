package Test;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.CartModel;
import models.InventoryPageModel;
import models.LogInPageModel;
import org.junit.*;

public class CartTest {

    Playwright playwright = Playwright.create();
    Browser browser = playwright.chromium().launch((new BrowserType.LaunchOptions().setHeadless(true)));
    Page page = browser.newPage();

    @Before
    public void setUp() {
        LogInPageModel logInPage = new LogInPageModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);
        logInPage.goTo("https://www.saucedemo.com/");
        //log in
        logInPage.logIn("standard_user", "secret_sauce");

        //Inventory Page
        inventoryPageModel.checkObjectText(inventoryPageModel.title, "PRODUCTS",true);
         //add to cart
        inventoryPageModel.addToCart("labs-backpack");
        inventoryPageModel.clickObject(inventoryPageModel.cartButton);

    }

    @Test
    public void AddedItemsAreDisplayedInCart() {
        CartModel cartModel = new CartModel(page);

        cartModel.checkCart("1", "Sauce Labs Backpack");
    }

    @Test
    public void removeItemFromCart() {
        CartModel cartModel = new CartModel(page);
        cartModel.checkCart("1", "Sauce Labs Backpack");
        cartModel.removeFromCart("backpack");
        cartModel.checkVisibility(cartModel.cart_quantity,false);
        cartModel.checkVisibility(cartModel.inventoryAssertName,false);
    }

    @Ignore         // Issues with test
    public void userCantProgressWithEmptyCart() {
        CartModel cartModel = new CartModel(page);
        cartModel.checkCart("1", "Sauce Labs Backpack");
        cartModel.removeFromCart("backpack");

        cartModel.checkVisibility(cartModel.cart_quantity,false);
        cartModel.checkVisibility(cartModel.inventoryAssertName,false);
        cartModel.checkVisibility(cartModel.checkOutButton,true);
    }

    @After
    public void closeBrowser() {
        browser.close();
    }
}
