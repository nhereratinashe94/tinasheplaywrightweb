package Test;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.CartModel;
import models.CheckOutModel;
import models.InventoryPageModel;
import models.LogInPageModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LogInPageTest {

    Playwright playwright = Playwright.create();
    Browser browser = playwright.chromium().launch((new BrowserType.LaunchOptions().setHeadless(true)));
    Page page = browser.newPage();
    @Before
    public void setUp() {
        page.navigate("https://www.saucedemo.com/");

    }
    @Test
    public void validUserCanLogInTest() {
        LogInPageModel logInPage = new LogInPageModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);
         //log in
        logInPage.logIn("standard_user", "secret_sauce");
        //Inventory Page
        logInPage.checkObjectText(inventoryPageModel.title, "PRODUCTS",true);
      }

    @Test
    public void lockedUserTest() {
        LogInPageModel logInPage = new LogInPageModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);
        //log in
        logInPage.logIn("locked_out_user", "secret_sauce");
         logInPage.checkVisibility(logInPage.lockedOutUser,true);
        //Inventory Page
        inventoryPageModel.checkVisibility(inventoryPageModel.title, false);
    }

    @Test
    public  void userCanLogOut(){

        LogInPageModel logInPage = new LogInPageModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);
          //log in
        logInPage.logIn("standard_user", "secret_sauce");
        //Inventory Page
         logInPage.checkObjectText(inventoryPageModel.title, "PRODUCTS",true);


        //Log out
        inventoryPageModel.clickObject(inventoryPageModel.burgerMenu);
        inventoryPageModel.clickObject(inventoryPageModel.logOutButton);
        inventoryPageModel.checkVisibility(logInPage.logInButton,true);
     }


    @After
    public void closeBrowser(){
        browser.close();
    }
}


