package Test;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import models.CartModel;
import models.CheckOutModel;
import models.InventoryPageModel;
import models.LogInPageModel;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InvertoryTest {

    Playwright playwright = Playwright.create();
    Browser browser = playwright.chromium().launch((new BrowserType.LaunchOptions().setHeadless(true)));
    Page page = browser.newPage();

    @Before
    public void setUp() {
        page.navigate("https://www.saucedemo.com/");
    }

    @Test
    public void userCanAdd_And_RemoveItemsToCartWithinInventoryPage(){
        LogInPageModel logInPage = new LogInPageModel(page);
        InventoryPageModel inventoryPageModel = new InventoryPageModel(page);
          //log in
        logInPage.logIn("standard_user", "secret_sauce");

        //Inventory Page
        Assert.assertEquals("PRODUCTS", inventoryPageModel.title.innerText());

        //add to cart
        inventoryPageModel.addToCart("labs-backpack");
        inventoryPageModel.addToCart("labs-bike-light");
        inventoryPageModel.checkObjectText(inventoryPageModel.cartButton,"2",true);

        //remove from cart
        inventoryPageModel.removeFromCart("labs-backpack");
        inventoryPageModel.checkObjectText(inventoryPageModel.cartButton,"1",true);
    }


    @After
    public void closeBrowser(){
        browser.close();
    }

}