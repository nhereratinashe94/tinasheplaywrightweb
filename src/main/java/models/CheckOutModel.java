package models;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.junit.Assert;

public class CheckOutModel {

    public final Locator inputFieldFirstName;
    public final Locator inputFieldLastName;
    public final Locator inputFieldZipCode;
    public final Locator continueButton;
    public final Locator cancelButton;
    public final Locator title;
    public final Locator finishButton;

    public  final Locator orderCompleteHeader;

    public final Locator  inventoryName;

    public final Locator  quantity;


    Page page;







    public CheckOutModel(Page page){
        this.inputFieldFirstName =page.locator("//input[@id='first-name']");
        this.inputFieldLastName = page.locator("//input[@id='last-name']");
        this.inputFieldZipCode =page.locator("//input[@id='postal-code']");
        this.continueButton = page.locator("//input[@id='continue']");
        this.cancelButton = page.locator("//button[@id='cancel']");
        this.title = page.locator("//span[@class='title']");
        this.finishButton = page.locator("//button[@id='finish']");
        this.inventoryName = page.locator("//div[@class='inventory_item_name']");
        this.quantity = page.locator("//div[@class='cart_quantity']");
        this.orderCompleteHeader =page.locator("//h2[@class='complete-header']");
        this.page=page;



    }

    public void addUserInfo(String firstName,String lastName,String zipCode){


        inputFieldFirstName.type(firstName);
        inputFieldLastName.type(lastName);
        inputFieldZipCode.type(zipCode);

    }
    public void assertTitle(String title){
        Assert.assertEquals(title,this.title.innerText());

    }

    public void finishOrder(){
        finishButton.click();
    }

    public void checkVisibility(Locator object ,Boolean visibilityStatus) {
        if (visibilityStatus.equals(true)) {
            Assert.assertTrue(object.isVisible());
        } else if (visibilityStatus.equals(false)) {
            Assert.assertTrue(object.isVisible());
        }
    }
    public void checkObjectText(Locator object,String expectedText,Boolean textStatus){

        if (textStatus.equals(true)){
            Assert.assertEquals(expectedText,object.innerText());
        }else if (textStatus.equals(false)){
            Assert.assertFalse(expectedText.contains(object.innerText()));
        }
    }
    public void clickObject(Locator object){

        object.click();

    }
}
