package models;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.Locator;
import org.junit.Assert;

public class LogInPageModel {

    private Page page;
    public final Locator passWordField;
    public final Locator userNameField;
    public final Locator logInButton;

     public final Locator lockedOutUser;


    public LogInPageModel(Page page) {

        this.passWordField = page.locator("//input[@id='password']");
        this.userNameField = page.locator("//input[@id='user-name']");
        this.logInButton = page.locator("//input[@id='login-button']");
        this.lockedOutUser = page.locator("//h3[@data-test='error']");
        this.page = page;
    }

    public void logIn(String username, String password) {
        passWordField.fill(password);
        userNameField.fill(username);
        logInButton.click();
    }

    public void  goTo(String url){
        page.navigate(url);


    }
    public void checkVisibility(Locator object,Boolean status) {

        if (status.equals(true)) {
            Assert.assertTrue(object.isVisible());
        } else if (status.equals(false)) {
            Assert.assertFalse(object.isVisible());

        }
    }
    public void checkObjectText(Locator object,String expectedText,Boolean textStatus){

        if (textStatus.equals(true)){
            Assert.assertEquals(expectedText,object.innerText());
        }else if (textStatus.equals(false)){
            Assert.assertFalse(expectedText.contains(object.innerText()));
        }
    }


}


