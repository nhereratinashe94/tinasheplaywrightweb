package models;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.junit.Assert;

public class CartModel {
    public final Locator cart_quantity;
    public  final  Locator checkOutButton;

    public final Locator  continueShoppingButton;

    public final Locator inventoryAssertName;

    Page page;

    public CartModel(Page page){
        this.cart_quantity = page.locator("//div[@class='cart_quantity']");

        this.checkOutButton = page.locator("//button[@id='checkout']");
        this.continueShoppingButton = page.locator("//button[@id='continue-shopping']");
        this.inventoryAssertName =page.locator("//div[@class='inventory_item_name']");
        this.page = page;
    }
    public void checkCart(String quantity ,String itemName ){
        Assert.assertEquals(quantity,cart_quantity.innerText());
        Assert.assertEquals(itemName, inventoryAssertName.innerText());
    }
  public  void removeFromCart(String item){

        page.locator("//button[@id='remove-sauce-labs-"+item+"']").click();


  }
    public void checkVisibility(Locator object ,Boolean visibilityStatus) {
        if (visibilityStatus.equals(true)) {
            Assert.assertTrue(object.isVisible());
        } else if (visibilityStatus.equals(false)) {
            Assert.assertFalse(object.isVisible());
        }
    }
    public void clickObject(Locator object){
        object.click();

    }
}
