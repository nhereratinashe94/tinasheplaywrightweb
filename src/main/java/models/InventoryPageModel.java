package models;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.junit.Assert;

public class InventoryPageModel {
    private Page page;
    public final Locator title;
    public final Locator cartButton;

    public final Locator burgerMenu;

    public final Locator logOutButton;


    public InventoryPageModel(Page page) {
        this.title = page.locator("//span[@class='title']");
        this.cartButton = page.locator("//a[@class='shopping_cart_link']");
        this.burgerMenu = page.locator("//button[@id='react-burger-menu-btn']");
        this.logOutButton =page.locator("//a[@id='logout_sidebar_link']");
        this.page = page;
    }


    public void addToCart(String item) {
        page.locator("//button[@id='add-to-cart-sauce-" + item + "']").click();

    }
    public void removeFromCart(String item){
        page.locator(" //button[@id='remove-sauce-" + item + "']").click();

    }
    public void clickObject(Locator object){
        object.click();

    }
    public void checkVisibility(Locator object ,Boolean visibilityStatus){
  if (visibilityStatus.equals(true)){
      Assert.assertTrue(object.isVisible());
  }else if (visibilityStatus.equals(false)){
      Assert.assertFalse(object.isVisible());
  }

    }
    public void checkObjectText(Locator object,String expectedText,Boolean textStatus){

        if (textStatus.equals(true)){
            Assert.assertEquals(expectedText,object.innerText());
        }else if (textStatus.equals(false)){
            Assert.assertFalse(expectedText.contains(object.innerText()));
        }
    }




}
