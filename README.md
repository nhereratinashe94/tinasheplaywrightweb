# Tinashe Web

This is a simple java maven framework that uses playwright and junit
to run  automated test scripts for https://www.saucedemo.com/.
Page object model is the  design pattern used.

Finally the project has a fully working ci/cd pipeline 

## Installation

Use the package manager [maven](https://pip.pypa.io/en/stable/) to install dependencies.

```bash
mvn install 
```

## Usage

```none
Run all tests :  mvn test
To run test headed change : setHeadless(false)
```

## License
[MIT](https://choosealicense.com/licenses/mit/)